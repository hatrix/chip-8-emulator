﻿import pygame
from pygame.locals import *
import time
from random import randrange
import os
import threading
import sys

# This is an attemp to write a Chip-8 emulator
# Python 3.2 and Pygame will be used


class Emulator:
    def __init__(self):
        pass

    def loadGame(self, game):       
        with open("./%s" % game, "rb") as file: # b is important -> binary
            content = file.read()
        
        i = 0
        for val in content:
            Vars.memory[512+i] = val
            i += 1
            
    def loop(self):
        condition = 1
        while condition:
            time.sleep(1/60)
            
            Vars.drawFlag = 0
            Chip8.emulateCycle()
            
            # Chip8.getKeys()

            # debug.write(hex(Chip8.opcode) + " : " + str(Chip8.V) + "\n")
            # debug.write("%s : %d\n\n" % (hex(Chip8.opcode), Chip8.I))
    
    def emulateCycle(self):          
        self.fetchOpcode()
        self.execOpcode()
        
        if Vars.delay_timer > 0:
            Vars.delay_timer -= 1
        if Vars.sound_timer > 0:
            if Vars.sound_timer == 1:
                # print("Beep !")
                print("\a")
            Vars.sound_timer -= 1
    
    def fetchOpcode(self):
        one = Vars.memory[Vars.pc]
        two = Vars.memory[Vars.pc + 1]
                
        Vars.opcode = one << 8 | two
        
        # print("opcode : %s ||" % hex(Vars.opcode), Vars.pc, hex(one), hex(two))
        # debug.write("opcode : %s\n" % hex(Vars.opcode)[2:])
        
        Vars.pc += 2

    def waitKey(self):
        keys = {K_1 : '1', K_2 : '2', K_3 : '3', K_4 : 'c', K_q : '4',\
                K_w : '5', K_e : '6', K_r : 'd', K_a : '7', K_s : '8',\
                K_d : '9', K_f : 'e', K_z : 'a', K_x : '0', K_c : 'b',\
                K_v : 'f'}
        
        condition = 1
        while condition:
            pygame.time.Clock().tick(60)
            for event in pygame.event.get():
                if event.type == QUIT:
                    quit()
                if event.type == KEYDOWN:
                    
                    if event.key in keys.keys():
                        continuer = 0
                        return keys[event.key]                   
    
    def execOpcode(self):
        opcode = hex(Vars.opcode)[2:]
               
        if opcode[0:2] == "e0" and  len(opcode) == 2:
            Screen.screen.fill((0,0,0))
            pygame.display.flip()
         
        elif opcode[0:2] == "ee" and len(opcode) == 2:
            for i in range(1,17):
                if Vars.stack[-i]:
                    Vars.pc = Vars.stack[-i]
                    Vars.stack_pointer -= 1
                    break
            
        elif opcode[0] == "1":
            Vars.pc = int(opcode[1:], 16)
       
        elif opcode[0] == "2":
            Vars.stack[Vars.stack_pointer] = Vars.pc
            Vars.stack_pointer += 1
            Vars.pc = int(opcode[1:], 16)

        elif opcode[0] == "3":
            if Vars.V[opcode[1]] == int(opcode[2:], 16):
                Vars.pc += 2

        elif opcode[0] == "4":
            if Vars.V[opcode[1]] != int(opcode[2:], 16):
                Vars.pc += 2
                
        elif opcode[0] == "5" and opcode[3] == "0":
            if Vars.V[opcode[1]] == Vars.V[opcode[2]]:
                Vars.pc += 2
        
        elif opcode[0] == "6":
            Vars.V[opcode[1]] = int(opcode[2:], 16)
        
        elif opcode[0] == "7":
            if Vars.V[opcode[1]] + int(opcode[2:], 16) > 255:
                    Vars.V[opcode[1]] = \
                        Vars.V[opcode[1]] + int(opcode[2:], 16) - 256
            else:
                Vars.V[opcode[1]] = \
                    Vars.V[opcode[1]] + int(opcode[2:], 16)
            # Vars.V[opcode[1]] = Vars.V[opcode[1]] + int(opcode[2:], 16)
            
        elif opcode[0] == "8":
            if opcode[3] == "0":
                Vars.V[opcode[1]] = Vars.V[opcode[2]]
                
            elif opcode[3] == "1":
                Vars.V[opcode[1]] = \
                     Vars.V[opcode[1]] | Vars.V[opcode[2]]
                
            elif opcode[3] == "2":
                Vars.V[opcode[1]] = \
                    Vars.V[opcode[1]] & Vars.V[opcode[2]]
                
            elif opcode[3] == "3":
                Vars.V[opcode[1]] = \
                     Vars.V[opcode[1]] ^ Vars.V[opcode[2]]
                
            elif opcode[3] == "4":
                # Set Vx = Vx + Vy, set VF = carry.
                if Vars.V[opcode[1]] + Vars.V[opcode[2]] > 255:
                    Vars.V[opcode[1]] = \
                        Vars.V[opcode[1]] + Vars.V[opcode[2]] - 256
                    Vars.V["f"] = 1
                else:
                    Vars.V[opcode[1]] = \
                        Vars.V[opcode[1]] + Vars.V[opcode[2]]
                    Vars.V["f"] = 0
            
            elif opcode[3] == "5":
                # Vx = Vx - Vy, if Vx > Vy, VF = 1
                # if Vy > Vx, VF = 0, start from 255
                # print(Vars.V[opcode[1]],Vars.V[opcode[2]], Vars.V[opcode[1]] - Vars.V[opcode[2]])
                if Vars.V[opcode[2]] > Vars.V[opcode[1]]:
                    Vars.V[opcode[1]] = \
                        Vars.V[opcode[1]] - Vars.V[opcode[2]] + 255
                    Vars.V["f"] = 0
                else:
                    Vars.V[opcode[1]] = \
                        Vars.V[opcode[1]] - Vars.V[opcode[2]]
                    Vars.V["f"] = 1
            
            elif opcode[3] == "6":
                Vars.V["f"] = int(str(Vars.V[opcode[1]])[-1])
                Vars.V[opcode[1]] = Vars.V[opcode[1]] // 2 # Vx / 2
                
            elif opcode[3] == "7":
                # Vx = Vy - Vx
                if Vars.V[opcode[2]] < Vars.V[opcode[1]]:
                    Vars.V[opcode[1]] = \
                        Vars.V[opcode[2]] - Vars.V[opcode[1]] + 255
                    Vars.V["f"] = 0
                else:
                    Vars.V[opcode[1]] = \
                        Vars.V[opcode[1]] - Vars.V[opcode[2]]
                    Vars.V["f"] = 1
                
            elif opcode[3] == "E":
                Vars.V["f"] = Vars.V[opcode[1]][0]
                Vars.V[opcode[1]] = Vars.V[opcode[1]] * 2 # Vx * 2
                        
        elif opcode[0] == "9":
            if Vars.V[opcode[1]] != Vars.V[opcode[2]]:
                Vars.pc += 2
                
        elif opcode[0] == "a":
            Vars.I = int(opcode[1:], 16)
        
        elif opcode[0] == "b":
            Vars.pc = int(opcode[1:], 16) + Vars.V["0"]
        
        elif opcode[0] == "c":
            Vars.V[opcode[1]] = randrange(256) & int(opcode[2:], 16)
        
        elif opcode[0] == "d":
            Vars.drawFlag = 0
            x = Vars.V[opcode[1]]
            y = Vars.V[opcode[2]]
            height = int(opcode[3], 16)
            
            # print(opcode[1], opcode[2])

            Vars.V["f"] = 0
            for i in range(height): # y
                pixel = Vars.memory[Vars.I + i]
                pixel = "0" * (8-len(bin(pixel)[2:])) + bin(pixel)[2:]
                
                for j in range(8): # x
                    if pixel != "00000000":
                        Vars.drawFlag = 1
                        self.writeGFX(pixel[j], x+j, y+i)
            
        elif opcode[0] == "e":
            if opcode[3] == "e":
                if Vars.key[int(opcode[1], 16)]:
                    Vars.pc += 2

            elif opcode[3] == "1":
                if not Vars.key[int(opcode[1], 16)]:
                    Vars.pc += 2
        
        elif opcode[0] == "f":
            if opcode[3] == "7":
                Vars.V[opcode[1]] = Vars.delay_timer
                
            elif opcode[3] == "a":
                Vars.V[opcode[1]] = int(Vars.waitKey(), 16)
                
            elif opcode[3] == "8":
                Vars.sound_timer = Vars.V[opcode[1]]
                
            elif opcode[3] == "e":
                Vars.I = Vars.I + Vars.V[opcode[1]]
            
            elif opcode[3] == "9":
                # Sets I to the location of the sprite for the character in VX. 
                # Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                Vars.I = Vars.V[opcode[1]] * 5
                # exit()
                
            elif opcode[3] == "3":
                number = Vars.V[opcode[1]]
                hundreds = number // 100
                tens = number // 10 - number // 100
                units = number - number // 10 * 10 - number // 100 * 100
                
                Vars.memory[Vars.I] = hundreds
                Vars.memory[Vars.I + 1] = tens
                Vars.memory[Vars.I + 2] = units

            elif opcode[3] == "5":
                if opcode[2] == "1":
                    Vars.delay_timer = Vars.V[opcode[1]]

                if opcode[2] == "5":
                    for i in range(int(opcode[1],16)+1):
                        Vars.memory[Vars.I+i] = Vars.V[str(i)]
                    
                if opcode[2] == "6":
                    # print(Vars.I, Vars.memory[Vars.I])
                    for i in range(int(opcode[1],16)+1):
                        Vars.V[hex(i)[2:]] = Vars.memory[Vars.I+i]

        else:
            print("Unknown opcode !")
       
           
    def writeGFX(self, pixel, x, y): # GFX !!!           
        if int(pixel): # if 1
        
            # print(x, y, pixel)
            
            if x > 63:
                # x -= (x//64) * 64
                x -= 64
            if y > 31:
                # y -= (y//32) * 32
                y -= 32

            # rect = pygame.Rect(x/64*512 , y/32*256, 8, 8)

            if Vars.gfx[x][y]:
                # pygame.draw.rect(Vars.screen, (0,0,0), rect)
                
                Vars.gfx[x][y] = 0
                Vars.V["f"] = 1                
            else:
                # pygame.draw.rect(Vars.screen, (255,255,255), rect)
                Vars.gfx[x][y] = 1
                # pygame.display.flip()    
       
    def getKeys(self):
        pass
        
class Variables:
    def __init__(self):
        self.opcode = int() # stores the current opcode which is 2 bytes long
        self.memory = dict() # Memory of the Chip-8, 4096
        self.V = {'0':0,'1':0,'2':0,'3':0,'4':0,'5':0,'6':0,'7':0,\
                  '8':0,'9':0,'a':0,'b':0,'c':0,'d':0,'e':0,'f':0} 
                  # 16 Registers of 1 byte each, V0, V1, ..., VE.
        # 16th is the carry flag : VF
        self.I = int() # Index Register, 4096
        self.pc = 512 # program counter, 4096

        self.gfx = [[int()]*32 for i in range(64)]
        
        self.delay_timer = int() # Registers that count down at 60Hz
        self.sound_timer = int()
        
        self.drawFlag = 0

        self.stack = [int()] * 16 
        # Stack to remember location before jump, 16 levels
        self.stack_pointer = int() # To remember which level is used
        self.key = [int()] * 16 # Keypad with 16 keys

        for i in range(4096):
            self.memory[i] = 0

        # load fontset
        chip8_fontset = self.fontset()
        for i in range(80):
            self.memory[i] = chip8_fontset[i]
            
    def fontset(self):
        chip8_fontset = [
          0xF0, 0x90, 0x90, 0x90, 0xF0, # 0
          0x20, 0x60, 0x20, 0x20, 0x70, # 1
          0xF0, 0x10, 0xF0, 0x80, 0xF0, # 2
          0xF0, 0x10, 0xF0, 0x10, 0xF0, # 3
          0x90, 0x90, 0xF0, 0x10, 0x10, # 4
          0xF0, 0x80, 0xF0, 0x10, 0xF0, # 5
          0xF0, 0x80, 0xF0, 0x90, 0xF0, # 6
          0xF0, 0x10, 0x20, 0x40, 0x40, # 7
          0xF0, 0x90, 0xF0, 0x90, 0xF0, # 8
          0xF0, 0x90, 0xF0, 0x10, 0xF0, # 9
          0xF0, 0x90, 0xF0, 0x90, 0x90, # A
          0xE0, 0x90, 0xE0, 0x90, 0xE0, # B
          0xF0, 0x80, 0x80, 0x80, 0xF0, # C
          0xE0, 0x90, 0x90, 0x90, 0xE0, # D
          0xF0, 0x80, 0xF0, 0x80, 0xF0, # E
          0xF0, 0x80, 0xF0, 0x80, 0x80] # F
        return chip8_fontset
        
class Graphics:
    def __init__(self):
        self.screen = pygame.display.set_mode((512,256))

    def loop(self):
        condition = 1
        while condition:
            pygame.time.Clock().tick(60)
            if Vars.drawFlag:
                self.draw()
        
    def draw(self):
        for i in range(len(Vars.gfx)):
            for j in range(len(Vars.gfx[i])):
                rect = pygame.Rect(i/64*512 , j/32*256, 8, 8)
                if Vars.gfx[i][j]:
                    pygame.draw.rect(Screen.screen, (255,255,255), rect)
                else:
                    pygame.draw.rect(Screen.screen, (0,0,0), rect)
        pygame.display.flip()  


pygame.init()        
        
Screen = Graphics()        
Vars = Variables()        
Chip8 = Emulator()


if len(sys.argv) == 2:
    Chip8.loadGame(sys.argv[1])
else:
    Chip8.loadGame("test/IBM Logo.ch8")

    
cpu = threading.Thread(None, Chip8.loop, None, (), {})
cpu.start()

display = threading.Thread(None, Screen.loop, None, (), {})
display.start()
    
# debug = open("debug.txt", "w")


    # debug.write("%s\n" % Chip8.V)

    # // If the draw flag is set, update the screen
    # if(myChip8.drawFlag)
      # drawGraphics();

    # // Store key press state (Press and Release)
    # myChip8.setKeys();	

# Memory Map
# 0x000-0x1FF - Chip 8 interpreter (contains font set in emu)
# 0x050-0x0A0 - Used for the built in 4x5 pixel font set (0-F)
# 0x200-0xFFF - Program ROM and work RAM

# Drawing is done in XOR mode and if a pixel is turned off as a result 
# of drawing, the VF register is set. This is used for collision detection.